
package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OperationsPage_Component extends Page {

	
	public OperationsPage_Component(WebDriver driver) {
        
		super(driver);
       // PageFactory.initElements(driver, this);
    }	
	
	 @FindBy(xpath = ".//*[@id='op_counterparties']//img")
	 public WebElement btnCounterparties;
	 	 
	 @FindBy(xpath = ".//*[@id='transfer_normal']//img")
	 public WebElement btnNormalTransfer;
	 
	 @FindBy(xpath = ".//*[@id='transfer_own']//img")
	 public WebElement btnOwnTransfer;
	 
	 public String info = "TO JEST BAZA DEV";
	
	
}


