
package pageObjects;

import org.apache.http.impl.client.EntityEnclosingRequestWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import static utils.Methods.*;
public class LoginPage_Component {
	
	
	
	 public LoginPage_Component(WebDriver driver) {
	        
	        PageFactory.initElements(driver, this);
	    }	
	
	
	 @FindBy(name = "nik")
	 public WebElement inpNik;
	 
	 @FindBy(id = "password")
	 public WebElement inpPassword;
	 
	 @FindBy(id = "login_button")
	 public WebElement btnLogin;
	 
	// By inpNik = By.name("nik");
	 
	  public void login(String login, String password){
		     
		  
		    sendText(this.inpNik, login);
		    sendText(this.inpPassword,password);
	        clickOn(this.btnLogin);
	        

	    }

}



